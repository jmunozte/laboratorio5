package com.academiasmoviles.lab5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var marcaSeleccion = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

/*
        val listaMarcas:MutableList<Marcas> = mutableListOf()

        listaMarcas.add(Marcas("Audi"))
        listaMarcas.add(Marcas("Nissan"))
        listaMarcas.add(Marcas("Toyota"))
        listaMarcas.add(Marcas("Volkswagen"))
        listaMarcas.add(Marcas("Kia"))
*/
        val listaMarcas = listOf("Audi","Nissan","Toyota","Volkswagen","Kia")

        val arrayAdapter = ArrayAdapter(this,R.layout.estilo_spinner,listaMarcas)
        spMarcas.adapter = arrayAdapter

        spMarcas.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                marcaSeleccion = listaMarcas[p2]
            }

        }

        btnConfirmar.setOnClickListener {

            val titular = edtTitular.text.toString()
            val anio = edtAnio.text.toString()
            val detalle = edtDetalle.text.toString()

            if (titular.isEmpty()) {
                Toast.makeText(this,"Debe ingresar un Titular", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (anio.isEmpty()) {
                Toast.makeText(this,"Debe ingresar un Año", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (detalle.isEmpty()) {
                Toast.makeText(this,"Debe ingresar un Detalle", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val regauto = RegAuto( titular, anio.toInt(), marcaSeleccion, detalle)


            crearModal(regauto).show()
        }

    }


    fun crearModal(registro:RegAuto) : AlertDialog{

        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val vi: View = inflater.inflate(R.layout.popup, null)
        builder.setView(vi)

        val tvNom: TextView = vi.findViewById(R.id.tvNombre)
        val tvAnio: TextView = vi.findViewById(R.id.tvAnio)
        val tvMarca: TextView = vi.findViewById(R.id.tvMarca)
        val tvDescripcion: TextView = vi.findViewById(R.id.tvDescripcion)
        val btnAceptar: Button = vi.findViewById(R.id.btnAceptar)

        tvNom.text = registro.titular
        tvAnio.text = registro.anio.toString()
        tvMarca.text = registro.marca
        tvDescripcion.text = registro.descripcion


        alertDialog = builder.create()
        btnAceptar.setOnClickListener {
            alertDialog.dismiss()
        }
        return alertDialog

    }


}